export const blue = {

  '--primary' : '#bab398',
  '--primary-variant': '#5f6a99',

  '--secondary': '#ffe165',
  '--secondary-variant': '#fff350',

  '--background': '#595649',
  '--surface': '#444039',
  '--dialog': '#6e6a58',
  '--cancel': '#3b3932',
  '--alt-surface': '#726a4c',
  '--alt-dialog': '#455a64',

  '--on-primary': '#000000',
  '--on-secondary': '#000000',
  '--on-background': '#1d1c18',
  '--on-surface': '#fff1d1',
  '--on-cancel': '#FFFFFF',

  '--green': 'lightgreen',
  '--red': 'red',
  '--yellow': 'yellow',
  '--blue': '#3f51b5',
  '--purple': '#9c27b0',
  '--light-green': '#80ba24',
  '--grey': '#BDBDBD',
  '--grey-light': '#9E9E9E',
  '--black': '#212121',
  '--moderator': '#37474f'
};

export const blue_meta = {

  'translation': {
    'name': {
      'en': 'YoRHa',
      'de': 'YoRHa'
    },
    'description': {
      'en': 'Sandy Theme',
      'de': 'Sandiges Theme'
    }
  },
  'isDark': true,
  'order': 3,
  'scale_desktop': 1,
  'scale_mobile': 1,
  'previewColor': 'background'

};
